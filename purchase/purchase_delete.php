<?php
require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Purchase.php';

session_start();

if(!isset($_GET['purchase_code'])) { $_SESSION['error'] = 'No Purchase id Given.'; }
else {
    $purchase = new Purchase();

    $id = $_GET['purchase_code'];
    if($purchase->remove($id)) { $_SESSION['success'] = 'Purchase Entry Successfully Deleted.'; }
    else { $_SESSION['error'] = 'An Error Occurred While Attempting to Remove a Purchase Entry.'; }
}
header('Location: index.php');
exit;
?>