<?php
$active_menu = 'purchases';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Group.php';
require_once '../lib/Brand.php';
require_once '../lib/Purchase.php';

session_start();

if(!isset($_GET['purchase_code'])) {
    header('Location: index.php');
    exit;
}

$id = (int)$_GET['purchase_code'];

$purchase = new Purchase();
$purchase_entry = $purchase->fetch($id);
if(!$purchase_entry) {
    $_SESSION['error'] = 'Purchase entry does not exist.';
    header("Location: index.php");
    exit;
}

$group = new Group();
$groups = $group->fetchAll();

$brand = new Brand();
$brands = $brand->fetchAll();

if(isset($_POST['purchase_edit'])) {
    $data = array(
        'purchase_code' => $id,
        'group_code' => $_POST['group_code'],
        'brand_code' => $_POST['brand_code'],
        'purchase_quantity' => $_POST['purchase_quantity'],
        'purchase_price' => $_POST['purchase_price']
    );
    if($purchase->save($data)) {
        $_SESSION['success'] = 'Purchase Entry Successfully Updated.';
        header('Location: index.php');
        exit;
    }
    else { $_SESSION['error'] = 'An Error Occured While Attempting to Update a Purchase Entry.'; }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-3">
                <form method="post" name="purchase_edit">
                    <div class="form-group">
                        <label for="group_code">Type</label>
                        <select name="group_code" class="form-control" required>
                        <?php foreach($groups as $group):?>
                        <option value="<?php echo $group['Group_Code'];?>" <?php echo $purchase_entry['Group_Code'] == $group['Group_Code'] ? 'selected' : '';?>><?php echo $group['Group Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="brand_code">Item</label>
                        <select name="brand_code" class="form-control" required>
                        <?php foreach($brands as $brand):?>
                        <option value="<?php echo $brand['Brand_Code'];?>" <?php echo $purchase_entry['Group_Code'] == $brand['Brand_Code'] ? 'selected' : '';?>><?php echo $brand['Brand Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="purchase_quantity">Quantiy</label>
                        <input type="text" class="form-control" name="purchase_quantity" id="purchase_quantity" value="<?php echo $purchase_entry['Purchase Quantity'];?>" placeholder="Purchase Quantity" required>
                    </div>

                    <div class="form-group">
                        <label for="purchase_price">Price</label>
                        <input type="text" class="form-control" name="purchase_price" id="purchase_price" value="<?php echo $purchase_entry['Purchase Price'];?>" placeholder="Purchase Price" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="purchase_edit" value="Save">
                    </div>
                </form>
            </div>
            <div class="col-md-9"></div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>