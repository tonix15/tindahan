<?php
$active_menu = 'purchases';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Purchase.php';

session_start();

$purchase = new Purchase();

$dateFrom = isset($_GET['date_from']) ? $_GET['date_from'] : null;
$dateTo = isset($_GET['date_to']) ? $_GET['date_to'] : null;
$listPurchases = $purchase->listPurchases($dateFrom, $dateTo);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<?php include_once '../assets/pieces/nav.tpl';?>

        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                <div class="form-group">
                    <div class="input-group date" id="date_from">
                        <input type="text" value="<?php echo !empty($dateFrom) ? $dateFrom : '';?>" class="form-control" placeholder="From" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                <div class="form-group">
                    <div class="input-group date" id="date_to">
                        <input type="text" value="<?php echo !empty($dateTo) ? $dateTo : '';?>" class="form-control" placeholder="To" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                <a href="#" class="btn btn-default view">View</a>
            </div>
        </div>

        <div class="form-group">
            <a href="new_purchase.php" class="btn btn-default">Add Purchase</a>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Item</th>
                    <th>Purchased Quantity</th>
                    <th>Purchased Date</th>
                    <th>Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($listPurchases as $item):?>
                <tr>
                    <td><?php echo $item['Group Description'];?></td>
                    <td><?php echo $item['Brand Description'];?></td>
                    <td><?php echo $item['Purchase Quantity'];?></td>
                    <td><?php echo $item['Purchase Date'];?></td>
                    <td><?php echo $item['Purchase Price'];?></td>
                    <td>
                        <a href="purchase_edit.php?purchase_code=<?php echo $item['Purchase_Code']?>&action=edit">Edit</a> |
                        <a href="purchase_delete.php?purchase_code=<?php echo $item['Purchase_Code']?>&action=delete">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
	</div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/moment-with-locales.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets/js/scripts.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>
</body>
</html>