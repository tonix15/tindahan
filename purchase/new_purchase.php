<?php
$active_menu = 'purchases';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Item.php';
require_once '../lib/Group.php';
require_once '../lib/Brand.php';
require_once '../lib/Purchase.php';

session_start();

$item = new Item();
$group = new Group();
$brand = new Brand();
$purchase = new Purchase();

$groups = $group->fetchAll();
$brands = $brand->fetchAll();
$purchases = $purchase->fetchAll();

if(isset($_POST['new_purchase'])) {
    $data = array(
        'group_code' => $_POST['group_code'],
        'brand_code' => $_POST['brand_code'],
        'purchase_quantity' => $_POST['purchase_quantity'],
        'purchase_price' => $_POST['purchase_price']
    );

    if($purchase->save($data)) {
        $i = $item->fetchByBrand($_POST['brand_code']);
        $quantity = (int)$i['Item Quantity'] + (int)$_POST['purchase_quantity'];
        $data = array('quantity' => $quantity, 'brand_code' => $_POST['brand_code']);

        if($item->updateItemQuantity($data)) { $_SESSION['success'] = "New Purchase Entry Successfully Added."; }
        else { $_SESSION['error'] = "An Error Occurred While Trying to Add a Purchase Entry."; }
    }
    else { $_SESSION['error'] = "An Error Occurred While Trying to Add a Purchase Entry."; }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-3">
                <form method="post" name="purchase_add">
                    <div class="form-group">
                        <label for="group_code">Type</label>
                        <select name="group_code" class="form-control" required>
                        <?php foreach($groups as $group):?>
                        <option value="<?php echo $group['Group_Code'];?>"><?php echo $group['Group Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="brand_code">Item</label>
                        <select name="brand_code" class="form-control" required>
                        <?php foreach($brands as $brand):?>
                        <option value="<?php echo $brand['Brand_Code'];?>"><?php echo $brand['Brand Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="purchase_quantity">Quantiy</label>
                        <input type="text" class="form-control" name="purchase_quantity" id="purchase_quantity" placeholder="Purchase Quantity">
                        <?php foreach($purchases as $purchase):?>
                        <input type="hidden" name="brand_<?php echo $purchase['Brand_Code'];?>" value="<?php echo $purchase['Purchase Quantity'];?>">
                        <?php endforeach;?>
                    </div>

                    <div class="form-group">
                        <label for="purchase_price">Price</label>
                        <input type="text" class="form-control" name="purchase_price" id="purchase_price" placeholder="Purchase Price">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="new_purchase" value="Save">
                    </div>
                </form>
            </div>
            <div class="col-md-9"></div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>
</body>
</html>