-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 07, 2018 at 02:45 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tindahandb`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
CREATE TABLE IF NOT EXISTS `brand` (
  `Brand_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Brand Description` varchar(255) NOT NULL,
  `Brand Size` varchar(20) NOT NULL,
  PRIMARY KEY (`Brand_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`Brand_Code`, `Brand Description`, `Brand Size`) VALUES
(1, 'Red Horse 1 L', 'Per Bottle'),
(2, 'Red Horse 500 mL', 'Per Bottle'),
(3, 'San Mig Pilsen 1 L', 'Per Bottle'),
(4, 'San Mig Pilsen 500 mL', 'Per Bottle'),
(5, 'San Mig Light 330 mL', 'Per Bottle'),
(6, 'Coke 1.5 L', 'Per Bottle'),
(7, 'Coke 1 L', 'Per Bottle'),
(8, 'Coke 12 oz', 'Per Bottle'),
(9, 'Coke 330 mL ', 'Per Can'),
(10, 'Sprite 1.5 L', 'Per Bottle'),
(11, 'Sprite 1 L', 'Per Bottle'),
(12, 'Sprite 12 oz', 'Per Bottle'),
(13, 'Sprite 330 mL', ' Per Can'),
(14, 'Royal 1.5 L', 'Per Bottle'),
(15, 'Royal 1 L', 'Per Bottle'),
(16, 'Royal 12 oz', 'Per Bottle'),
(17, 'Royal 330 mL', 'Per Can');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `Group_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Group Description` varchar(255) NOT NULL,
  PRIMARY KEY (`Group_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`Group_Code`, `Group Description`) VALUES
(1, 'Beer'),
(2, 'Softdrinks');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `Item_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Group_Code` int(11) NOT NULL,
  `Brand_Code` int(11) NOT NULL,
  `Item Quantity` int(11) NOT NULL,
  `Item Date` date NOT NULL,
  `Item Cost` double NOT NULL,
  `Item Price` double NOT NULL,
  PRIMARY KEY (`Item_Code`),
  KEY `Group_Code` (`Group_Code`) USING BTREE,
  KEY `Brand_Code` (`Brand_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
CREATE TABLE IF NOT EXISTS `purchase` (
  `Purchase_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Group_Code` int(11) NOT NULL,
  `Brand_Code` int(11) NOT NULL,
  `Purchase Quantity` int(11) NOT NULL,
  `Purchase Date` date NOT NULL,
  `Purchase Price` double NOT NULL,
  PRIMARY KEY (`Purchase_Code`),
  KEY `Group_Code` (`Group_Code`),
  KEY `Brand_Code` (`Brand_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
CREATE TABLE IF NOT EXISTS `sales` (
  `Sales_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Item_Code` int(11) NOT NULL,
  `Sales Quantity` int(11) NOT NULL,
  `Sales Date` int(11) NOT NULL,
  PRIMARY KEY (`Sales_Code`),
  KEY `Item_Code` (`Item_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`Group_Code`) REFERENCES `groups` (`Group_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `item_ibfk_2` FOREIGN KEY (`Brand_Code`) REFERENCES `brand` (`Brand_Code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`Group_Code`) REFERENCES `groups` (`Group_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `purchase_ibfk_2` FOREIGN KEY (`Brand_Code`) REFERENCES `brand` (`Brand_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`Item_Code`) REFERENCES `item` (`Item_Code`);
COMMIT;

ALTER TABLE brand
ADD UNIQUE INDEX unique_brand_index (`Brand Description`);

ALTER TABLE groups
ADD UNIQUE INDEX unique_groups_index (`Group Description`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
