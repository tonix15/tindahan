<nav class="navbar navbar-default" role="navigation">
  	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Tindahan</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
			<li <?php echo $active_menu == 'item' ? 'class="active"':'';?>><a href="<?php echo Utility::host() . 'item/';?>">Items</a></li>
			<li <?php echo $active_menu == 'sales' ? 'class="active"':'';?>><a href="<?php echo Utility::host() . 'sales/';?>">Sales</a></li>
			<li <?php echo $active_menu == 'purchases' ? 'class="active"':'';?>><a href="<?php echo Utility::host() . 'purchase';?>">Purchases</a></li>
			<li <?php echo $active_menu == 'brand' ? 'class="active"':'';?>><a href="<?php echo Utility::host() . 'brand/';?>">Brand</a></li>
			<li <?php echo $active_menu == 'groups' ? 'class="active"':'';?>><a href="<?php echo Utility::host() . 'groups/';?>">Groups</a></li>
		</ul>
		<form class="navbar-form navbar-right" role="search">
			<div class="form-group">
    			<input type="search" class="form-control" placeholder="Search">
    		</div>
		</form>
	</div><!-- /.navbar-collapse -->
</nav>