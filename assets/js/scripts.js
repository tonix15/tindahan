$(function() {
    $('#date_from, #date_to').datetimepicker({format: 'YYYY-MM-DD'});

    // Linking the from and to date pickers
    $('#date_to').datetimepicker({useCurrent: false});
    $('#date_from').on('dp.change', function(e) {
        $('#date_to').data('DateTimePicker').minDate(e.date);
    });
    $('#date_to').on('dp.change', function(e) {
        $('#date_from').data('DateTimePicker').maxDate(e.date);
    });

    $('.view').on('click', function(e){
        e.preventDefault();
        var date_from = encodeURI($('#date_from > .form-control').val());
        var date_to = encodeURI($('#date_to > .form-control').val());
        window.location.search = "date_from=" + date_from + "&date_to=" + date_to;
    });
});