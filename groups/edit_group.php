<?php
$active_menu = 'groups';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Group.php';

session_start();

$group = new Group();

if(!isset($_GET['group_code'])) {
    header('Location: index.php');
    exit;
}

$id = (int)$_GET['group_code'];
$g = $group->fetch($id);

if(isset($_POST['edit_group'])) {
    $data = array(
        'group_code' => $_POST['group_code'],
        'group_description' => $_POST['group_desc']
    );
    if($group->save($data)) {
        $_SESSION['success'] = 'New Group Added Successfully.';
        header('Location: index.php');
        exit;
    }
    else { $_SESSION['error'] = 'An Error Occurred while adding a Group.'; }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-4">
                <form method="post" name="edit_group">
                    <input type="hidden" name="group_code" value="<?php echo $g['Group_Code'];?>">
                    <div class="form-group">
                        <label for="item_group">Description</label>
                        <input type="text" name="group_desc" class="form-control" placeholder="Group Description" value="<?php echo $g['Group Description'];?>" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="edit_group" value="Save">
                    </div>
                </form>
            </div>
            <div class="col-md-8"></div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>