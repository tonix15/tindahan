<?php
require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Group.php';

session_start();

if(!isset($_GET['group_code'])) { $_SESSION['error'] = 'No Group id Given.'; }
else {
    $group = new Group();

    $id = $_GET['group_code'];
    if($group->remove($id)) { $_SESSION['success'] = 'Group Entry Successfully Deleted.'; }
    else { $_SESSION['error'] = 'Cannot Remove Group Entry, has Child Entries.'; }
}
header('Location: index.php');
exit;
?>