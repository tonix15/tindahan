<?php
$active_menu = 'groups';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Group.php';

session_start();

$group = new Group();
if(isset($_POST['new_group'])) {
    $data = array('group_description' => $_POST['group_desc']);
    if($group->groupExists($data['group_description'])) {
        $_SESSION['error'] = 'Group Entry Already Exists.';
    }
    else {
        if($group->save($data)) {
            $_SESSION['success'] = 'New Group Added Successfully.';
        }
        else { $_SESSION['error'] = 'An Error Occurred while adding a new Group.'; }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-4">
                <h2>New Group</h2>
                <form method="post" name="new_group">
                    <div class="form-group">
                        <label for="item_group">Description</label>
                        <input type="text" name="group_desc" class="form-control" placeholder="Group Description" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="new_group" value="Save">
                    </div>
                </form>
            </div>
            <div class="col-md-8"></div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>