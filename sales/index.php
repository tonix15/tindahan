<?php
$active_menu = 'sales';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Sales.php';

session_start();

$sales = new Sales();

$dateFrom = isset($_GET['date_from']) ? $_GET['date_from'] : null;
$dateTo = isset($_GET['date_to']) ? $_GET['date_to'] : null;
$listSales = $sales->listSales($dateFrom, $dateTo);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<?php include_once '../assets/pieces/nav.tpl';?>

		<div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                <div class="form-group">
                    <div class="input-group date" id="date_from">
                        <input type="text" value="<?php echo !empty($dateFrom) ? $dateFrom : '';?>" class="form-control" placeholder="From" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                <div class="form-group">
                    <div class="input-group date" id="date_to">
                        <input type="text" value="<?php echo !empty($dateTo) ? $dateTo : '';?>" class="form-control" placeholder="To" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                <a href="#" class="btn btn-default view">View</a>
            </div>
        </div>

        <table class="table table-striped">
			<thead>
				<tr>
					<th>Item</th>
					<th>Quantity Sold</th>
					<th>Date Sold</th>
                    <th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($listSales as $item):?>
				<tr>
					<td><?php echo $item['Brand Description'];?></td>
					<td><?php echo $item['Sales Quantity'];?></td>
					<td><?php echo $item['Sales Date'];?></td>
                    <td><a href="sales_delete.php?sale_id=<?php echo $item['Sales_Code'];?>&action=delete" class="btn">Delete</a></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/moment-with-locales.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.js"></script>
	<script src="../assets/js/scripts.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>