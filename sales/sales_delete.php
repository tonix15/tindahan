<?php
require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Sales.php';

session_start();

if(!isset($_GET['sale_id'])) { $_SESSION['error'] = 'No Sale id Given.'; }
else {
    $sale = new Sales();

    $id = $_GET['sale_id'];
    if($sale->remove($id)) { $_SESSION['success'] = 'Sale Entry Successfully Deleted.'; }
    else { $_SESSION['error'] = 'An Error Occurred While Attempting to Remove a Sales Entry.'; }
}
header('Location: index.php');
exit;
?>