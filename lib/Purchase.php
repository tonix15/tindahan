<?php
class Purchase extends Base {
    private $conn;

    public function __construct() {
        $this->conn = parent::connect();
    }

    public function save($data = array()) {
        $group_code = $data['group_code'];
        $brand_code = $data['brand_code'];
        $purchase_quantity = $data['purchase_quantity'];
        $purchase_price = $data['purchase_price'];

        if(!isset($data['purchase_code'])) {
            $query = "INSERT INTO purchase(Group_Code, Brand_Code, `Purchase Quantity`, `Purchase Date`, `Purchase Price`) ";
            $query .= "VALUES({$group_code}, {$brand_code}, {$purchase_quantity}, current_date, {$purchase_price})";
        }
        else {
            $query = "UPDATE purchase SET Group_Code = {$group_code}, Brand_Code = {$brand_code}, `Purchase Quantity` = {$purchase_quantity}, ";
            $query .= "`Purchase Date` = current_date, `Purchase Price` = {$purchase_price} WHERE Purchase_Code = {$data['purchase_code']}";
        }
        return $this->conn->query($query) ? true : false;
    }

    public function fetch($id) {
        $query = "SELECT * FROM purchase WHERE Purchase_Code = {$id}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function fetchByBrand($brand_id) {
        $query = "SELECT * FROM purchase WHERE Brand_Code = {$brand_id}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function fetchAll() {
        $query = "SELECT * FROM purchase";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function remove($id) {
        $query = "DELETE FROM purchase WHERE Purchase_Code = {$id}";
        return $this->conn->query($query) ? true : false;
    }

    public function listPurchases($dateFrom = null, $dateTo = null) {
        $query = "SELECT g.`Group Description`, b.`Brand Description`, p.Purchase_Code , p.`Purchase Quantity`, ";
        $query .= "p.`Purchase Date`, p.`Purchase Price` FROM purchase p ";
        $query .= "LEFT JOIN groups g ON p.Group_Code = g.Group_Code ";
        $query .= "LEFT JOIN brand b ON p.Brand_Code = b.Brand_Code ";
        if($dateFrom != '' && $dateTo != '') {
            $query .= "WHERE p.`Purchase Date` >= '{$dateFrom}' AND p.`Purchase Date` <= '{$dateTo}'";
        }
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function itemExists($brand) {
        $query = "SELECT * FROM purchase WHERE Brand_Code = {$brand}";
        $stmt = $this->conn->query($query);
        return $stmt->num_rows;
    }

    public function sumQuantity($brand) {
        $query = "SELECT SUM(`Purchase Quantity`) as Quantity FROM purchase ";
        $query .= "WHERE Brand_Code = {$brand}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function itemQuantityByLatestPurchase($brand) {
        $query = "SELECT SUM(`Purchase Quantity`) as Quantity, `Purchase Date` FROM purchase ";
        $query .= "WHERE Brand_Code = {$brand} ORDER BY `Purchase Date` DESC";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }
}
?>