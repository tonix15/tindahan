<?php
class Database {
    protected function connect() {
        // localhost will connect using a socket
        // while 127.0.0.1 will connect using TCP/IP
        $conn = new mysqli('127.0.0.1', 'root', 'asdf@1234', 'tindahandb');
        if($conn->connect_errno) {
            die('Failed to Connect to DB: (' . $conn->connect_errno . ')' . $conn->connect_error);
        }
        return $conn;
    }
}
?>