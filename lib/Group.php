<?php
class Group extends Base {
    private $conn;

    public function __construct() {
        $this->conn = parent::connect();
    }

    public function save($data = array()) {
        $desc = $data['group_description'];
        if(!isset($data['group_code'])) {
            $query = "INSERT INTO groups(`Group Description`) VALUES('{$desc}')";
        }
        else {
            $query = "UPDATE groups SET `Group Description` = '{$desc}' WHERE Group_Code = {$data['group_code']}";
        }
        return $this->conn->query($query) ? true : false;
    }

    public function fetch($id) {
        $query = "SELECT * FROM groups WHERE Group_Code = {$id}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function fetchAll() {
        $query = "SELECT * FROM groups";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function remove($id) {

    }

    public function groupExists($group) {
        $query = "SELECT Group_Code, `Group Description` FROM groups WHERE `Group Description` LIKE '%{$group}'";
        $stmt = $this->conn->query($query);
        return $stmt->num_rows;
    }
}
?>