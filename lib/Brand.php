<?php
class Brand extends Base {
    private $conn;

    public function __construct() {
        $this->conn = parent::connect();
    }

    public function save($data = array()) {
        $desc = $data['brand_description'];
        $size = $data['brand_size'];
        if(!isset($data['brand_code'])) {
            $query = "INSERT INTO brand(`Brand Description`, `Brand Size`) ";
            $query .= "VALUES('{$desc}', '{$size}')";
        }
        else {
            $query = "UPDATE brand SET `Brand Description` = '{$desc}', `Brand Size` = '{$size}' ";
            $query .= "WHERE Brand_Code = {$data['brand_code']}";
        }
        return $this->conn->query($query) ? true : false;
    }

    public function fetch($id) {
        $query = "SELECT * FROM brand WHERE Brand_Code = {$id}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function fetchAll(){
        $query = "SELECT * FROM brand ORDER BY `Brand Description`";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function remove($id){
        $query = "DELETE FROM brand WHERE Brand_Code = {$id}";
        return $this->conn->query($query) ? true : false;
    }

    public function listAllBrandSize() {
        $query = "SELECT Brand_Code, `Brand Size` FROM brand Group By `Brand Size`;";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function brandExists($brand) {
        $query = "SELECT Brand_Code, `Brand Description` FROM brand WHERE `Brand Description` LIKE '%{$brand}%'";
        $stmt = $this->conn->query($query);
        return $stmt->num_rows;
    }
}
?>