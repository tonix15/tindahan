<?php
class Item extends Base {
    private $conn;

    public function __construct() {
        $this->conn = parent::connect();
    }

    public function save($data = array()) {
        if(!isset($data['item_code'])) {
            $query = "INSERT INTO item(Group_Code, Brand_Code, `Item Quantity`, `Item Date`, `Item Cost`, `Item Price`) ";
            $query .= "VALUES({$data['group_code']}, {$data['brand_code']}, {$data['item_quantity']}, current_date, {$data['item_cost']}, {$data['item_price']})";
        }
        else {
            $query = "UPDATE item SET Group_Code = {$data['group_code']}, Brand_Code = {$data['brand_code']}, `Item Quantity` = {$data['item_quantity']}, ";
            $query .= "`Item Date` = current_date, `Item Cost` = {$data['item_cost']}, `Item Price` = {$data['item_price']} ";
            $query .= "WHERE Item_Code = {$data['item_code']}";
        }
        return $this->conn->query($query) ? true : false;
    }

    public function fetch($id) {
        $query = "SELECT * FROM item WHERE Item_Code = {$id}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function fetchAll() {
        $query = "SELECT * FROM item";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function fetchByBrand($brand_id) {
        $query = "SELECT * FROM item WHERE Brand_Code = {$brand_id}";
        $stmt = $this->conn->query($query);
        return $stmt->fetch_assoc();
    }

    public function remove($id) {
        $query = "DELETE FROM item WHERE Item_Code = {$id}";
        return $this->conn->query($query) ? true : false;
    }

    public function itemExists($brand) {
        $query = "SELECT Item_Code, Group_Code, Brand_Code FROM item WHERE Brand_Code = {$brand}";
        $stmt = $this->conn->query($query);
        return $stmt->num_rows;
    }

    public function updateItemQuantity($data = array()) {
        $query = "UPDATE item SET `Item Quantity` = {$data['quantity']} WHERE Brand_Code = {$data['brand_code']}";
        return $this->conn->query($query) ? true : false;
    }

    public function getAllItems() {
        $query = "SELECT g.`Group Description`, b.`Brand Description`, b.`Brand Size`, i.Item_Code, i.`Item Quantity`, ";
        $query .= "i.`Item Date`, i.`Item Price`, i.`Item Cost`FROM item i INNER JOIN groups g on i.Group_Code = g.Group_Code ";
        $query .= "INNER JOIN brand b on i.Brand_Code = b.Brand_Code";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function getAllItemsById($id) {
        $query = "SELECT g.Group_Code, g.`Group Description`, b.Brand_Code, b.`Brand Description`, b.`Brand Size`, i.Item_Code, ";
        $query .= "i.`Item Quantity`, i.`Item Date`, i.`Item Price`, i.`Item Cost`FROM item i ";
        $query .= "INNER JOIN groups g on i.Group_Code = g.Group_Code ";
        $query .= "INNER JOIN brand b on i.Brand_Code = b.Brand_Code WHERE i.Item_Code = {$id}";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function saveItems($data = array()) {
        $query = "UPDATE item SET Group_Code = {$data['group_code']}, `Item Quantity` = {$data['item_quantity']}, ";
        $query .= "`Item Date` = current_date, `Item Cost` = {$data['item_cost']}, `Item Price` = {$data['item_price']} ";
        $query .= "WHERE Item_Code = {$data['item_code']}";
        $stmt = $this->conn->query($query);
        return $stmt;
    }

    public function getAllAvailableBrands() {
        $query = "SELECT b.*, i.Item_Code, i.`Item Quantity` FROM brand b ";
        $query .= "INNER JOIN item i ON b.Brand_Code = i.Brand_Code ";
        $stmt = $this->conn->query($query);
        return $stmt;
    }
}
?>