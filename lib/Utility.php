<?php
class Utility {
    public static final function dump($data){
        echo '<pre>';
        var_dump($data);
        echo '<pre>';
    }

    public static final function host() {
        $str = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/assignment/';
        return $str;
    }
}
?>