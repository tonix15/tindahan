<?php
abstract class Base extends Database{
    abstract public function save($data = array());
    abstract public function fetch($id);
    abstract public function fetchAll();
    abstract public function remove($id);
}
?>