<?php
class Sales extends Base {
    private $conn;

    public function __construct() {
        $this->conn = parent::connect();
    }

    public function save($data = array()) {
        $item_code = $data['item_code'];
        $quantity = $data['quantity'];
        $prev_quantity = $data['prev_quantity'];

        $query = "INSERT INTO sales(Item_Code, `Sales Quantity`, `Sales Date`) ";
        $query .= "VALUES({$item_code}, {$quantity}, current_date)";

        $this->conn->query($query);

        $query = "UPDATE item SET `Item Quantity` = ({$prev_quantity} - {$quantity}) ";
        $query .= "WHERE Brand_Code = {$data['brand_code']}";
        return $this->conn->query($query) ? true : false;
    }

    public function fetch($id) {

    }

    public function fetchAll() {

    }

    public function remove($id) {
        $query = "DELETE FROM sales WHERE Sales_Code = {$id}";
        return $this->conn->query($query) ? true : false;
    }

    public function listSales($dateFrom = null, $dateTo = null) {
        $query = "SELECT s.Sales_Code, b.`Brand Description`, s.`Sales Quantity`, s.`Sales Date` FROM sales s ";
        $query .= "LEFT JOIN item i ON s.Item_Code = i.Item_Code ";
        $query .= "LEFT JOIN brand b ON i.Brand_Code = b.Brand_Code ";
        if($dateFrom != '' && $dateTo != '') {
            $query .= "WHERE s.`Sales Date` >= '{$dateFrom}' AND s.`Sales Date` <= '{$dateTo}'";
        }
        $stmt = $this->conn->query($query);
        return $stmt;
    }
}
?>