<?php
$active_menu = 'brand';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Brand.php';

session_start();

$brand = new Brand();
if(isset($_POST['new_brand'])) {
    $data = array(
        'brand_description' => $_POST['brand_desc'],
        'brand_size' => $_POST['brand_size']
    );
    if($brand->brandExists($data['brand_description'])) {
        $_SESSION['error'] = 'Brand Entry Already Exists.';
    }
    else {
        if($brand->save($data)) {
            $_SESSION['success'] = 'New Brand Added Successfully.';
            header('Location: index.php');
            exit;
        }
        else { $_SESSION['error'] = 'An Error Occurred while adding a new Brand.'; }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-4">
                <form method="post" name="new_brand">
                    <div class="form-group">
                        <label for="item_group">Description</label>
                        <input type="text" name="brand_desc" class="form-control" placeholder="Brand Description" required>
                    </div>

                    <div class="form-group">
                        <label for="item_group">Size</label>
                        <input type="text" name="brand_size" class="form-control" placeholder="Brand Size" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="new_brand" value="Save">
                    </div>
                </form>
            </div>
            <div class="col-md-8"></div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>