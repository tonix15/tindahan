<?php
$active_menu = 'brand';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Brand.php';

session_start();

$brand = new Brand();
$brands = $brand->fetchAll();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="form-group">
            <a href="new_brand.php" class="btn btn-default">New Brand</a>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Size</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($brands as $brand):?>
                <tr>
                    <td><?php echo $brand['Brand Description'];?></td>
                    <td><?php echo $brand['Brand Size'];?></td>
                    <td>
                        <a href="edit_brand.php?brand_code=<?php echo $brand['Brand_Code'];?>&action=edit">Edit</a> |
                        <a href="brand_delete.php?brand_code=<?php echo $brand['Brand_Code'];?>&action=delete">Delete</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>