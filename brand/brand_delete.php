<?php
require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Brand.php';

session_start();

if(!isset($_GET['brand_code'])) { $_SESSION['error'] = 'No Brand id Given.'; }
else {
    $brand = new Brand();

    $id = $_GET['brand_code'];
    if($brand->remove($id)) { $_SESSION['success'] = 'Brand Entry Successfully Deleted.'; }
    else { $_SESSION['error'] = 'Cannot Remove Brand Entry, has Child Entries.'; }
}
header('Location: index.php');
exit;
?>