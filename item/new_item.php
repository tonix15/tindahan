<?php
$active_menu = 'item';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Item.php';
require_once '../lib/Group.php';
require_once '../lib/Brand.php';
require_once '../lib/Purchase.php';

session_start();

$item = new Item();
$group = new Group();
$brand = new Brand();
$purchase = new Purchase();

$groups = $group->fetchAll();
$brands = $brand->fetchAll();
$purchases = $purchase->fetchAll();

if(isset($_POST['new_item'])) {
    $item = new Item();

    $group_code = $_POST['group_code'];
    $brand_code = $_POST['brand_code'];
    $item_cost = $_POST['item_cost'];
    $item_price = $_POST['item_price'];

    $data = array(
        'group_code' => $group_code,
        'brand_code' => $brand_code,
        'item_cost' => $item_cost,
        'item_price' => $item_price
    );

    // Check if Selected Item is already in Items table.
    if($item->itemExists($brand_code)){
        $_SESSION['error'] = 'Item Already Exists.';

        // if Brand already exists in item table, update the quantity with the latest entry from the purchase table

    }
    else {
        $item_num = $purchase->itemExists($brand_code);

        // if Brand is not yet in item table and also not in purchase table; quantity = 0
        if($item_num <= 0) {
            $data['item_quantity'] = 0;
        }

        // if Brand is not yet in item table, but exists in purchase table; quantity = sum of rows of a given brand in purchase table
        elseif ($item_num > 0) {
            $p = $purchase->sumQuantity($brand_code);
            $data['item_quantity'] = is_null($p['Quantity']) ? 0 : $p['Quantity'];
        }

        if($item->save($data)) { $_SESSION['success'] = 'New Item Successfully Added.'; }
        else { $_SESSION['error'] = 'An Error Occurred While Adding an Item Entry.'; }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-3">
                <form method="post" name="item_add">
                    <div class="form-group">
                        <label for="group_code">Group</label>
                        <select name="group_code" class="form-control" required>
                        <?php foreach($groups as $group):?>
                        <option value="<?php echo $group['Group_Code'];?>"><?php echo $group['Group Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="brand_code">Brand</label>
                        <select name="brand_code" class="form-control" required>
                        <?php foreach($brands as $brand):?>
                        <option value="<?php echo $brand['Brand_Code'];?>"><?php echo $brand['Brand Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="item_cost">Cost</label>
                        <input type="text" class="form-control" name="item_cost" id="item_cost" placeholder="Item Cost">
                    </div>

                    <div class="form-group">
                        <label for="item_price">Price</label>
                        <input type="text" class="form-control" name="item_price" id="item_price" placeholder="Item Price">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="new_item" value="Save">
                    </div>
                </form>
            </div>
            <div class="col-md-9"></div>
        </div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>
</body>
</html>