<?php
$active_menu = 'item';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Group.php';
require_once '../lib/Brand.php';
require_once '../lib/Sales.php';
require_once '../lib/Item.php';

session_start();

$group = new Group();
$groups = $group->fetchAll();

$brand = new Brand();
$brands = $brand->fetchAll();
$brand_sizes = $brand->listAllBrandSize();

$sale = new Sales();

$item = new Item();
$items = $item->fetchAll();
$available_items = $item->getAllAvailableBrands();

if(isset($_POST['item_sell'])) {
    list($brand_code, $item_code, $prev_quantity) = explode('|', $_POST['item_brand']);

    $item_quantity = $_POST['item_quantity'];

    if($prev_quantity <= 0 || ($item_quantity > $prev_quantity)) {
        $_SESSION['error'] = 'Item Has Not Enough Stock.';
    }
    else {
        $data = array(
            'item_code' => $item_code,
            'quantity' => $_POST['item_quantity'],
            'prev_quantity' =>  $prev_quantity,
            'brand_code' => $brand_code
        );
        if($sale->save($data)) {
            $_SESSION['success'] = 'Item Successfully Sold.';
            header('Location: index.php');
            exit;
        }
        else { $_SESSION['error'] = 'An Error Occured While Attempting to Sell an item.'; }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-3">
                <form method="post">
                    <div class="form-group">
                        <label for="item_brand">Item</label>
                        <select name="item_brand" class="form-control" required>
                        <?php foreach($available_items as $item):?>
                        <option value="<?php echo $item['Brand_Code'] . '|' . $item['Item_Code'] . '|' . $item['Item Quantity'];?>"><?php echo $item['Brand Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="item_quantity">Quantity</label>
                        <input type="text" class="form-control" name="item_quantity" id="item_quantity" placeholder="Quantity of Item to Sell." required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="item_sell" value="Sell">
                    </div>
                </form>
            </div>
            <div class="col-md-9"></div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>