<?php
$active_menu = 'item';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Item.php';

session_start();

$item = new Item();
$items = $item->getAllItems();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<?php include_once '../assets/pieces/nav.tpl';?>
		<div class="row">
			<div class="col-md-12">
				<a href="new_item.php" class="btn btn-default">Add New Item</a>
				<a href="sell_item.php" class="btn btn-default">Sell Item</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Group</th>
							<th>Item</th>
							<th>Size</th>
							<th>Quantity</th>
							<th>Last Updated</th>
							<th>Price</th>
							<th>Cost</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($items as $item):?>
						<tr>
							<td><?php echo $item['Group Description'];?></td>
							<td><?php echo $item['Brand Description'];?></td>
							<td><?php echo $item['Brand Size'];?></td>
							<td><?php echo $item['Item Quantity'];?></td>
							<td><?php echo $item['Item Date'];?></td>
							<td><?php echo $item['Item Price'];?></td>
							<td><?php echo $item['Item Cost'];?></td>
							<td>
                                <a href="item_edit.php?item_id=<?php echo $item['Item_Code'];?>&action=edit" class="btn">Edit</a> |
                                <a href="item_delete.php?item_id=<?php echo $item['Item_Code'];?>&action=delete" class="btn">Delete</a>
                            </td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/moment.min.js"></script>
	<script src="../assets/js/moment-with-locales.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/bootstrap-datetimepicker.js"></script>
	<script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['success'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['success'];?>",
            {
                globalPosition: 'top center',
                className: 'success'
            }
        );
    </script>
    <?php endif; unset($_SESSION['success']);?>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>