<?php
$active_menu = 'item';
//require_once '../lib/Autoloader.php';

require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Item.php';
require_once '../lib/Group.php';
require_once '../lib/Brand.php';

session_start();

if(!isset($_GET['item_id'])) {
    header('Location: index.php');
    exit;
}

$id = (int)$_GET['item_id'];

$item = new Item();
$items = $item->getAllItemsById($id);

$group = new Group();
$groups = $group->fetchAll();

$brand = new Brand();
$brands = $brand->fetchAll();
$brand_size = $brand->listAllBrandSize();
$date = date('Y-m-d');

if(isset($_POST['Save'])) {
    $data = array(
        'item_code' => $id,
        'group_code' => $_POST['item_group'],
        'item_quantity' => $_POST['item_quantity'],
        'item_cost' => $_POST['item_cost'],
        'item_price' => $_POST['item_price']
    );
    if($item->saveItems($data)) {
        $_SESSION['success'] = 'Item Successfully Updated.';
        header('Location: ' . Utility::host());
        exit;
    }
    else { $_SESSION['error'] = 'An Error Occured while attempting to update an item.'; }
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tindahan Project - <?php echo ucwords($active_menu);?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<?php include_once '../assets/pieces/nav.tpl';?>
        <div class="row">
            <div class="col-md-3">
                <form action="#" method="post" name="item_edit">
                    <?php foreach($items as $item):?>
                    <div class="form-group">
                        <label for="item_group">Group</label>
                        <select name="item_group" class="form-control">
                        <?php foreach($groups as $group):?>
                        <option value="<?php echo $group['Group_Code'];?>"<?php echo ($item['Group Description'] == $group['Group Description']) ? ' selected' : '';?>><?php echo $group['Group Description'];?></option>
                        <?php endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <h4>Brand</h4>
                        <label for="brand_desc"><?php echo $item['Brand Description'];?></label>
                    </div>

                    <div class="form-group">
                        <h4>Item Size</h4>
                        <label for="item_size"><?php echo $item['Brand Size'];?></label>
                    </div>

                    <div class="form-group">
                        <label for="item_quantity">Quantity</label>
                        <input type="text" class="form-control" name="item_quantity" id="item_quantity" value="<?php echo $item['Item Quantity'];?>">
                    </div>

                    <div class="form-group">
                        <h4>Last Updated</h4>
                        <label for="last_updated"><?php echo $item['Item Date'];?></label>
                    </div>

                    <div class="form-group">
                        <label for="item_price">Price</label>
                        <input type="text" class="form-control" name="item_price" id="item_price" value="<?php echo $item['Item Price'];?>">
                    </div>

                    <div class="form-group">
                        <label for="item_cost">Cost</label>
                        <input type="text" class="form-control" name="item_cost" id="item_cost" value="<?php echo $item['Item Cost'];?>">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="Save" value="Save">
                    </div>
                    <?php endforeach;?>
                </form>
            </div>
            <div class="col-md-9"></div>
        </div>
	</div>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/notify.min.js"></script>

    <?php if(isset($_SESSION['error'])):?>
    <script type="text/javascript">
        $.notify(
            "<?php echo $_SESSION['error'];?>",
            {
                globalPosition: 'top center',
                className: 'error'
            }
        );
    </script>
    <?php endif; unset($_SESSION['error']);?>
</body>
</html>