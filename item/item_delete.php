<?php
require_once '../lib/Utility.php';
require_once '../lib/Database.php';
require_once '../lib/Base.php';
require_once '../lib/Item.php';

session_start();

if(!isset($_GET['item_id'])) { $_SESSION['error'] = 'No Item id Given.'; }
else {
    $item = new Item();

    $id = $_GET['item_id'];
    if($item->remove($id)) { $_SESSION['success'] = 'Item Entry Successfully Deleted.'; }
    else { $_SESSION['error'] = 'Item Entry Currently Exists in Sales, Cannot be Removed.'; }
}
header('Location: index.php');
exit;
?>